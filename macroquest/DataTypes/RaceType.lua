---@class RaceType
__RaceType = {
    ---@return number
    ---The ID of the race
    ID = function()end,
    ---@return string
    ---The name of the race
    Name = function()end,
}