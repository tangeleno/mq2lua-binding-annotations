---@class DeityType
__DeityType = {
    ---@return number
    ---The deity's ID #
    ID = function()end,
    ---@return string
    ---The full deity name
    Name = function()end,
    ---@return string
    ---The team name
    Team = function()end,
}