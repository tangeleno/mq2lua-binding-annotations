---@class MountType
__MountType = {
    ---@return number
    ---Where on the keyring list 
    Index = function()end,
    ---@return string
    ---Name of the mount in XX slot
    Name = function()end,
}