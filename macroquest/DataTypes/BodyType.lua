---@class BodyType
__BodyType = {
    ---@return number
    ---The ID of the body type
    ID = function()end,
    ---@return string
    ---The full name of the body type
    Name = function()end,
}