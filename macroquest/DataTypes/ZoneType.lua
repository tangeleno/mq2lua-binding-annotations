---@class ZoneType
__ZoneType = {
    ---@return number
    ---ID of the zone
    ID = function()end,
    ---@return string
    ---Full zone name
    Name = function()end,
    ---@return string
    ---Short zone name
    ShortName = function()end,
    ---@return number
    ---???
    ZoneFlags = function()end,
}