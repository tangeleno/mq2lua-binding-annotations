---@class ARGBType
__ARGBType = {
    ---@return number
    ---Alpha
    A=function()end,
    ---@return number
    ---Red
    R=function()end,
    ---@return number
    ---Green
    G=function()end,
    ---@return number
    ---Blue
    B=function()end,
    ---@return number
    ---The integer formed by ARGB
    Int=function()end,
}