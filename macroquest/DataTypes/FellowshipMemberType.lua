---@class FellowshipMemberType
__FellowshipMemberType = {
    ---@return ClassType
    Class = function()end,
    ---@return number
    Level = function()end,
    ---@return TicksType
    LastOn = function()end,
    ---@return string
    Name = function()end,
    ---@return ZoneType
    Zone = function()end,
}