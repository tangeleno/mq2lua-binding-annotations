---@diagnostic disable: duplicate-index
---@class FriendType
__FriendType = {
    ---@return boolean
    ---Returns TRUE if name is on your friend list
    Friend = function(name)end,
    ---@return string
    ---Returns the name of friend list member #
    Friend = function(index)end,
}