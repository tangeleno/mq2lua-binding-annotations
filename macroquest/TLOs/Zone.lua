---@diagnostic disable: duplicate-index
---@class TLOZone
__TLOZone = {
    ---@return CurrentZoneType
    Zone = function()end,
    ---@return ZoneType
    Zone = function(zoneId)end,
    ---@return ZoneType
    Zone = function(shortName)end,
}